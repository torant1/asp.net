﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using exopoo.domaine;

namespace exopoo.ui
{
    public partial class Test : System.Web.UI.Page
    {
        //On rend static la variable lst_personne pour permettre
        // l'acces durant plusieurs POSTBACK
        public static List<Personne> lst_personne = new List<Personne>();

        protected void Page_Load(object sender, EventArgs e)
        {
           // Personne p;
            //if (IsPostBack)
            //{
            //    lst_personne = new List<Personne>();
            //}
            //else {
            //    //lst_personne = new List<Personne>();
            //    lblmessage.Text = "On cree";
            //}
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String nom, prenom, sexe, pays;
            Personne p = null;

            nom = txtnom.Text.Trim();
            prenom = txtprenom.Text.Trim();
            sexe = ddlsexe.SelectedItem.Value;
            pays = ddlpays.SelectedItem.Value;

            // Creation de l'objet personne
            p = new Personne(nom,prenom,sexe,pays);
            

           // lblmessage.Text=p.ToString();
            lst_personne.Add(p);
            GridView1.DataSource = lst_personne;
            GridView1.DataBind();

        }
    }
}