﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="exopoo.ui.Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <th colspan="3">Enregistrer une personne</th>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblnom" runat="server" Text="Entrer nom"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtnom" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtnom" ErrorMessage="Champ nom vide" ForeColor="#CC0000" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtnom" ErrorMessage="Le nom est incorrecte" ForeColor="#CC0000" SetFocusOnError="True" ValidationExpression="^[a-zA-Z][a-zA-Z\. ]+$">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblprenom" runat="server" Text="Entrer prenom"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtprenom" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtprenom" ErrorMessage="Champ nom vide" ForeColor="#CC0000">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblsexe" runat="server" Text="Entrer sexe"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlsexe" runat="server">
                        <asp:ListItem>Masculin</asp:ListItem>
                        <asp:ListItem>Feminin</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblpays" runat="server" Text="Entrer votre pays"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlpays" runat="server">
                        <asp:ListItem>Haiti</asp:ListItem>
                        <asp:ListItem Value="USA">Etats-Unis</asp:ListItem>
                        <asp:ListItem>Afrique du sud</asp:ListItem>
                        <asp:ListItem>Afrique du nord</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                   
                </td>
                <td>
                    
                    <asp:Button ID="Button1" runat="server" Text="Enregistrer" OnClick="Button1_Click" />
                    
                </td>
                <td>&nbsp;</td>
            </tr>
             <tr>
                <td colspan="2">

                  

                    <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>

                  

                </td>
            </tr>
            <tr>
                <td colspan="2">

                    <asp:Label ID="lblmessage" runat="server" ForeColor="#33CC33"></asp:Label>

                </td>
            </tr>

        </table>
        
    
    </div>
    </form>
</body>
</html>
