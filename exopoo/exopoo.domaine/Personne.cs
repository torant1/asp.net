﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exopoo.domaine
{
    public class Personne
    {
        //Attributs
         private string nom;
         private string prenom;
         private string sexe;
         private string pays;

        

        // propietés
         public string Nom
         {
             get { return nom; }
             set
             {
                 if (!String.IsNullOrEmpty(value.Trim()))
                 {
                     nom = value;
                 }
                 else
                 {
                     //On genere une exception
                     throw new Exception("Le non ne peut etre vide");
                 }
             }

         }



        public string Prenom
         {
             get { return prenom; }
             set { prenom = value; }
         }
        
        

        public string Sexe
         {
             get { return sexe; }
             set { sexe = value; }
         }
         


        public string Pays
         {
             get { return pays; }
             set { pays = value; }
         }


       // Les contructeurs
        public Personne(string n,string p, string s,string pa){
            this.Nom = n.Trim();
            this.Prenom = p.Trim();
            this.Sexe = s.Trim();
            this.Pays = pa.Trim();
        }

        //Methode utiles
            public override string ToString(){
              //override c'est une redefinition dde la superclasse personne
                return string.Format("{0} {1}",nom.ToUpper(),prenom);
            }
             
        
       }

}
