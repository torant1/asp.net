﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using exopoo.domaine;
using exopoo.dal;
namespace exopoo.controleur
{
    public class ControleurPersonne
    {
        public static int SaveToFile(string adresse, Personne p) {
            int result = 0;
            try
            {
                //On fait le controle
                //Si la chaine adresse n'est ni vide ni nulle et  l'objet P n'est pas null
                if (!string.IsNullOrEmpty(adresse) && p != null)
                {
                    result = DataSaver.SaveToFile(adresse, p);
                }
            }
            catch (Exception ex) {
                //ON fait monter l'exception generee dans la couche au dessus
                //Pour traitement
                throw new Exception(ex.Message);
            }
            return result;
        }
    }
}
