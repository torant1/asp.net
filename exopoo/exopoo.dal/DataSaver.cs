﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using exopoo.domaine;


namespace exopoo.dal
{
    public class DataSaver
    {

        public static int SaveToFile(String adresse , Personne p) {
            StreamWriter ecrivain=null;
            int result = 0;
            try
            {
                //On cree l'objet StreamWriter qui va permettre d'ecrire dans le fichier
                ecrivain = new StreamWriter(adresse, true);
               // ecrivain.AutoFlush = true;
                //On met l'objet Personne sous format CSV qui consiste a separer
                //chaque champ par un virgule ou un point virgule sous une seule ligne
                // qui representera un enregistrement
                string tocsv = String.Format("{0},{1},{2},{3}", p.Nom, p.Prenom, p.Sexe, p.Pays);
                //ON ecrit la ligne dans le fichier
                ecrivain.WriteLine(tocsv);
                //On valide l'entree
                ecrivain.Flush();
                //ON precise que le fichier a ete sauvegarde
                //En passant la valeur de la variable result a 1.
                result = 1;
                
                //Si il y a des erreurs dans le traitement du fichier
            }
            catch (IOException fnex)
            {
                //ON fait monter l'exception generee dans la couche au dessus
                //Pour traitement
                throw new Exception(fnex.Message);
            }
            finally {
                if (ecrivain != null) {
                    //On ferme le fichier
                    ecrivain.Close();
                }
            }

            

            return result;
        } 

  


    }
}
